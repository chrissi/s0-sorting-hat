EESchema Schematic File Version 4
LIBS:sorting_hat-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2200 1300 0    50   Input ~ 0
SIG1
Text HLabel 2950 1200 2    50   Output ~ 0
SIG1_NC
Text HLabel 2950 1400 2    50   Output ~ 0
SIG1_NO
Wire Wire Line
	2350 1300 2300 1300
Wire Wire Line
	2750 1200 2800 1200
Wire Wire Line
	2950 1400 2850 1400
Wire Wire Line
	2350 1750 2300 1750
Wire Wire Line
	2300 1750 2300 1300
Connection ~ 2300 1300
Wire Wire Line
	2300 1300 2200 1300
Wire Wire Line
	2750 1650 2800 1650
Wire Wire Line
	2800 1650 2800 1200
Connection ~ 2800 1200
Wire Wire Line
	2800 1200 2950 1200
Wire Wire Line
	2850 1400 2850 1850
Wire Wire Line
	2850 1850 2750 1850
Connection ~ 2850 1400
Wire Wire Line
	2850 1400 2750 1400
$Comp
L powerport:+5V0 #PWR023
U 1 1 5A970653
P 4100 1150
F 0 "#PWR023" H 4100 1000 50  0001 C CNN
F 1 "+5V0" H 4115 1323 50  0000 C CNN
F 2 "" H 4100 1150 50  0000 C CNN
F 3 "" H 4100 1150 50  0000 C CNN
	1    4100 1150
	1    0    0    -1  
$EndComp
$Comp
L nexperia:BAS70 D6
U 1 1 5A970727
P 4350 1350
F 0 "D6" V 4304 1428 50  0000 L CNN
F 1 "BAS70" V 4395 1428 50  0000 L CNN
F 2 "TO_SMD:SOT-23" H 4400 1150 50  0001 C CNN
F 3 "/ptx/archive/library/datasheets/Diodes/Nexperia_BAS70-Series.pdf" H 4350 1450 50  0001 C CNN
F 4 "Nexperia" H 4450 1550 60  0001 C CNN "Manufacturer"
F 5 "BAS70-215, BAS70-235" H 4550 1650 60  0001 C CNN "MPN"
	1    4350 1350
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 1150 4100 1150
Wire Wire Line
	4100 1550 4350 1550
$Comp
L nexperia:BSS138BK Q1
U 1 1 5A970D66
P 4100 1850
F 0 "Q1" H 4191 1953 60  0000 L CNN
F 1 "BSS138BK" H 4191 1847 60  0000 L CNN
F 2 "TO_SMD:SOT-23" H 2350 3250 60  0001 C CNN
F 3 "/ptx/archive/library/datasheets/Transistors/Nexperia_BSS138BK_N-MOSFET.pdf" H 2350 3250 60  0001 C CNN
F 4 "BSS138BK" H 2450 3350 60  0001 C CNN "MPN"
F 5 "Nexperia" H 2550 3450 60  0001 C CNN "Manufacturer"
	1    4100 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 1600 4100 1550
$Comp
L powerport:GND #PWR024
U 1 1 5A970F64
P 4100 2200
F 0 "#PWR024" H 4100 2100 50  0001 C CNN
F 1 "GND" H 4115 2077 50  0000 C CNN
F 2 "" H 4100 2200 50  0000 C CNN
F 3 "" H 4100 2200 50  0000 C CNN
	1    4100 2200
	1    0    0    -1  
$EndComp
$Comp
L Res_1Percent_E24_0603_100mW_-40C-90C:10k_0603_100mW_1%_E24_Chip-Resistor R11
U 1 1 5A971107
P 3650 2000
F 0 "R11" H 3720 2046 50  0000 L CNN
F 1 "10k_0603_100mW_1%_E24_Chip-Resistor" V 3514 2001 50  0001 C CNN
F 2 "Resistors_SMD:R_0603" V 3580 2000 50  0001 C CNN
F 3 "" V 3730 2000 50  0001 C CNN
F 4 "any" V 3830 2100 60  0001 C CNN "MPN"
F 5 "any" V 3930 2200 60  0001 C CNN "Manufacturer"
F 6 "10k" H 3720 1955 50  0000 L CNN "DisplayValue"
	1    3650 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2000 4100 2150
Wire Wire Line
	3650 2150 4100 2150
Connection ~ 4100 2150
Wire Wire Line
	4100 2150 4100 2200
Wire Wire Line
	3800 1850 3650 1850
Connection ~ 3650 1850
Wire Wire Line
	3650 1850 3500 1850
Text HLabel 3500 1850 0    50   Input ~ 0
SIG1_IN
Text HLabel 2200 2950 0    50   Input ~ 0
SIG2
Text HLabel 2950 2850 2    50   Output ~ 0
SIG2_NC
Text HLabel 2950 3050 2    50   Output ~ 0
SIG2_NO
Wire Wire Line
	2350 2950 2300 2950
Wire Wire Line
	2750 2850 2800 2850
Wire Wire Line
	2950 3050 2850 3050
Wire Wire Line
	2350 3400 2300 3400
Wire Wire Line
	2300 3400 2300 2950
Connection ~ 2300 2950
Wire Wire Line
	2300 2950 2200 2950
Wire Wire Line
	2750 3300 2800 3300
Wire Wire Line
	2800 3300 2800 2850
Connection ~ 2800 2850
Wire Wire Line
	2800 2850 2950 2850
Wire Wire Line
	2850 3050 2850 3500
Wire Wire Line
	2850 3500 2750 3500
Connection ~ 2850 3050
Wire Wire Line
	2850 3050 2750 3050
$Comp
L powerport:+5V0 #PWR025
U 1 1 5A97186D
P 4100 2800
F 0 "#PWR025" H 4100 2650 50  0001 C CNN
F 1 "+5V0" H 4115 2973 50  0000 C CNN
F 2 "" H 4100 2800 50  0000 C CNN
F 3 "" H 4100 2800 50  0000 C CNN
	1    4100 2800
	1    0    0    -1  
$EndComp
$Comp
L nexperia:BAS70 D7
U 1 1 5A971875
P 4350 3000
F 0 "D7" V 4304 3078 50  0000 L CNN
F 1 "BAS70" V 4395 3078 50  0000 L CNN
F 2 "TO_SMD:SOT-23" H 4400 2800 50  0001 C CNN
F 3 "/ptx/archive/library/datasheets/Diodes/Nexperia_BAS70-Series.pdf" H 4350 3100 50  0001 C CNN
F 4 "Nexperia" H 4450 3200 60  0001 C CNN "Manufacturer"
F 5 "BAS70-215, BAS70-235" H 4550 3300 60  0001 C CNN "MPN"
	1    4350 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 2800 4100 2800
Wire Wire Line
	4100 3200 4350 3200
$Comp
L nexperia:BSS138BK Q2
U 1 1 5A971881
P 4100 3500
F 0 "Q2" H 4191 3603 60  0000 L CNN
F 1 "BSS138BK" H 4191 3497 60  0000 L CNN
F 2 "TO_SMD:SOT-23" H 2350 4900 60  0001 C CNN
F 3 "/ptx/archive/library/datasheets/Transistors/Nexperia_BSS138BK_N-MOSFET.pdf" H 2350 4900 60  0001 C CNN
F 4 "BSS138BK" H 2450 5000 60  0001 C CNN "MPN"
F 5 "Nexperia" H 2550 5100 60  0001 C CNN "Manufacturer"
	1    4100 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3250 4100 3200
$Comp
L powerport:GND #PWR026
U 1 1 5A97188A
P 4100 3850
F 0 "#PWR026" H 4100 3750 50  0001 C CNN
F 1 "GND" H 4115 3727 50  0000 C CNN
F 2 "" H 4100 3850 50  0000 C CNN
F 3 "" H 4100 3850 50  0000 C CNN
	1    4100 3850
	1    0    0    -1  
$EndComp
$Comp
L Res_1Percent_E24_0603_100mW_-40C-90C:10k_0603_100mW_1%_E24_Chip-Resistor R12
U 1 1 5A971893
P 3650 3650
F 0 "R12" H 3720 3696 50  0000 L CNN
F 1 "10k_0603_100mW_1%_E24_Chip-Resistor" V 3514 3651 50  0001 C CNN
F 2 "Resistors_SMD:R_0603" V 3580 3650 50  0001 C CNN
F 3 "" V 3730 3650 50  0001 C CNN
F 4 "any" V 3830 3750 60  0001 C CNN "MPN"
F 5 "any" V 3930 3850 60  0001 C CNN "Manufacturer"
F 6 "10k" H 3720 3605 50  0000 L CNN "DisplayValue"
	1    3650 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3650 4100 3800
Wire Wire Line
	3650 3800 4100 3800
Connection ~ 4100 3800
Wire Wire Line
	4100 3800 4100 3850
Wire Wire Line
	3800 3500 3650 3500
Connection ~ 3650 3500
Wire Wire Line
	3650 3500 3500 3500
Text HLabel 3500 3500 0    50   Input ~ 0
SIG2_IN
Text HLabel 2200 4500 0    50   Input ~ 0
SIG3
Text HLabel 2950 4400 2    50   Output ~ 0
SIG3_NC
Text HLabel 2950 4600 2    50   Output ~ 0
SIG3_NO
Wire Wire Line
	2350 4500 2300 4500
Wire Wire Line
	2750 4400 2800 4400
Wire Wire Line
	2950 4600 2850 4600
Wire Wire Line
	2350 4950 2300 4950
Wire Wire Line
	2300 4950 2300 4500
Connection ~ 2300 4500
Wire Wire Line
	2300 4500 2200 4500
Wire Wire Line
	2750 4850 2800 4850
Wire Wire Line
	2800 4850 2800 4400
Connection ~ 2800 4400
Wire Wire Line
	2800 4400 2950 4400
Wire Wire Line
	2850 4600 2850 5050
Wire Wire Line
	2850 5050 2750 5050
Connection ~ 2850 4600
Wire Wire Line
	2850 4600 2750 4600
$Comp
L powerport:+5V0 #PWR027
U 1 1 5A97207B
P 4100 4350
F 0 "#PWR027" H 4100 4200 50  0001 C CNN
F 1 "+5V0" H 4115 4523 50  0000 C CNN
F 2 "" H 4100 4350 50  0000 C CNN
F 3 "" H 4100 4350 50  0000 C CNN
	1    4100 4350
	1    0    0    -1  
$EndComp
$Comp
L nexperia:BAS70 D8
U 1 1 5A972083
P 4350 4550
F 0 "D8" V 4304 4628 50  0000 L CNN
F 1 "BAS70" V 4395 4628 50  0000 L CNN
F 2 "TO_SMD:SOT-23" H 4400 4350 50  0001 C CNN
F 3 "/ptx/archive/library/datasheets/Diodes/Nexperia_BAS70-Series.pdf" H 4350 4650 50  0001 C CNN
F 4 "Nexperia" H 4450 4750 60  0001 C CNN "Manufacturer"
F 5 "BAS70-215, BAS70-235" H 4550 4850 60  0001 C CNN "MPN"
	1    4350 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 4350 4100 4350
Wire Wire Line
	4100 4750 4350 4750
$Comp
L nexperia:BSS138BK Q3
U 1 1 5A97208F
P 4100 5050
F 0 "Q3" H 4191 5153 60  0000 L CNN
F 1 "BSS138BK" H 4191 5047 60  0000 L CNN
F 2 "TO_SMD:SOT-23" H 2350 6450 60  0001 C CNN
F 3 "/ptx/archive/library/datasheets/Transistors/Nexperia_BSS138BK_N-MOSFET.pdf" H 2350 6450 60  0001 C CNN
F 4 "BSS138BK" H 2450 6550 60  0001 C CNN "MPN"
F 5 "Nexperia" H 2550 6650 60  0001 C CNN "Manufacturer"
	1    4100 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4800 4100 4750
$Comp
L powerport:GND #PWR028
U 1 1 5A972098
P 4100 5400
F 0 "#PWR028" H 4100 5300 50  0001 C CNN
F 1 "GND" H 4115 5277 50  0000 C CNN
F 2 "" H 4100 5400 50  0000 C CNN
F 3 "" H 4100 5400 50  0000 C CNN
	1    4100 5400
	1    0    0    -1  
$EndComp
$Comp
L Res_1Percent_E24_0603_100mW_-40C-90C:10k_0603_100mW_1%_E24_Chip-Resistor R13
U 1 1 5A9720A1
P 3650 5200
F 0 "R13" H 3720 5246 50  0000 L CNN
F 1 "10k_0603_100mW_1%_E24_Chip-Resistor" V 3514 5201 50  0001 C CNN
F 2 "Resistors_SMD:R_0603" V 3580 5200 50  0001 C CNN
F 3 "" V 3730 5200 50  0001 C CNN
F 4 "any" V 3830 5300 60  0001 C CNN "MPN"
F 5 "any" V 3930 5400 60  0001 C CNN "Manufacturer"
F 6 "10k" H 3720 5155 50  0000 L CNN "DisplayValue"
	1    3650 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 5200 4100 5350
Wire Wire Line
	3650 5350 4100 5350
Connection ~ 4100 5350
Wire Wire Line
	4100 5350 4100 5400
Wire Wire Line
	3800 5050 3650 5050
Connection ~ 3650 5050
Wire Wire Line
	3650 5050 3500 5050
Text HLabel 3500 5050 0    50   Input ~ 0
SIG3_IN
Text HLabel 6200 1350 0    50   Input ~ 0
SIG4
Text HLabel 6950 1250 2    50   Output ~ 0
SIG4_NC
Text HLabel 6950 1450 2    50   Output ~ 0
SIG4_NO
Wire Wire Line
	6350 1350 6300 1350
Wire Wire Line
	6750 1250 6800 1250
Wire Wire Line
	6950 1450 6850 1450
Wire Wire Line
	6350 1800 6300 1800
Wire Wire Line
	6300 1800 6300 1350
Connection ~ 6300 1350
Wire Wire Line
	6300 1350 6200 1350
Wire Wire Line
	6750 1700 6800 1700
Wire Wire Line
	6800 1700 6800 1250
Connection ~ 6800 1250
Wire Wire Line
	6800 1250 6950 1250
Wire Wire Line
	6850 1450 6850 1900
Wire Wire Line
	6850 1900 6750 1900
Connection ~ 6850 1450
Wire Wire Line
	6850 1450 6750 1450
$Comp
L powerport:+5V0 #PWR029
U 1 1 5A975BC2
P 8100 1200
F 0 "#PWR029" H 8100 1050 50  0001 C CNN
F 1 "+5V0" H 8115 1373 50  0000 C CNN
F 2 "" H 8100 1200 50  0000 C CNN
F 3 "" H 8100 1200 50  0000 C CNN
	1    8100 1200
	1    0    0    -1  
$EndComp
$Comp
L nexperia:BAS70 D9
U 1 1 5A975BCA
P 8350 1400
F 0 "D9" V 8304 1478 50  0000 L CNN
F 1 "BAS70" V 8395 1478 50  0000 L CNN
F 2 "TO_SMD:SOT-23" H 8400 1200 50  0001 C CNN
F 3 "/ptx/archive/library/datasheets/Diodes/Nexperia_BAS70-Series.pdf" H 8350 1500 50  0001 C CNN
F 4 "Nexperia" H 8450 1600 60  0001 C CNN "Manufacturer"
F 5 "BAS70-215, BAS70-235" H 8550 1700 60  0001 C CNN "MPN"
	1    8350 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	8350 1200 8100 1200
Wire Wire Line
	8100 1600 8350 1600
$Comp
L nexperia:BSS138BK Q4
U 1 1 5A975BD6
P 8100 1900
F 0 "Q4" H 8191 2003 60  0000 L CNN
F 1 "BSS138BK" H 8191 1897 60  0000 L CNN
F 2 "TO_SMD:SOT-23" H 6350 3300 60  0001 C CNN
F 3 "/ptx/archive/library/datasheets/Transistors/Nexperia_BSS138BK_N-MOSFET.pdf" H 6350 3300 60  0001 C CNN
F 4 "BSS138BK" H 6450 3400 60  0001 C CNN "MPN"
F 5 "Nexperia" H 6550 3500 60  0001 C CNN "Manufacturer"
	1    8100 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 1650 8100 1600
$Comp
L powerport:GND #PWR030
U 1 1 5A975BDF
P 8100 2250
F 0 "#PWR030" H 8100 2150 50  0001 C CNN
F 1 "GND" H 8115 2127 50  0000 C CNN
F 2 "" H 8100 2250 50  0000 C CNN
F 3 "" H 8100 2250 50  0000 C CNN
	1    8100 2250
	1    0    0    -1  
$EndComp
$Comp
L Res_1Percent_E24_0603_100mW_-40C-90C:10k_0603_100mW_1%_E24_Chip-Resistor R14
U 1 1 5A975BE8
P 7650 2050
F 0 "R14" H 7720 2096 50  0000 L CNN
F 1 "10k_0603_100mW_1%_E24_Chip-Resistor" V 7514 2051 50  0001 C CNN
F 2 "Resistors_SMD:R_0603" V 7580 2050 50  0001 C CNN
F 3 "" V 7730 2050 50  0001 C CNN
F 4 "any" V 7830 2150 60  0001 C CNN "MPN"
F 5 "any" V 7930 2250 60  0001 C CNN "Manufacturer"
F 6 "10k" H 7720 2005 50  0000 L CNN "DisplayValue"
	1    7650 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2050 8100 2200
Wire Wire Line
	7650 2200 8100 2200
Connection ~ 8100 2200
Wire Wire Line
	8100 2200 8100 2250
Wire Wire Line
	7800 1900 7650 1900
Connection ~ 7650 1900
Wire Wire Line
	7650 1900 7500 1900
Text HLabel 7500 1900 0    50   Input ~ 0
SIG4_IN
Text HLabel 6200 2900 0    50   Input ~ 0
SIG5
Text HLabel 6950 2800 2    50   Output ~ 0
SIG5_NC
Text HLabel 6950 3000 2    50   Output ~ 0
SIG5_NO
Wire Wire Line
	6350 2900 6300 2900
Wire Wire Line
	6750 2800 6800 2800
Wire Wire Line
	6950 3000 6850 3000
Wire Wire Line
	6350 3350 6300 3350
Wire Wire Line
	6300 3350 6300 2900
Connection ~ 6300 2900
Wire Wire Line
	6300 2900 6200 2900
Wire Wire Line
	6750 3250 6800 3250
Wire Wire Line
	6800 3250 6800 2800
Connection ~ 6800 2800
Wire Wire Line
	6800 2800 6950 2800
Wire Wire Line
	6850 3000 6850 3450
Wire Wire Line
	6850 3450 6750 3450
Connection ~ 6850 3000
Wire Wire Line
	6850 3000 6750 3000
$Comp
L powerport:+5V0 #PWR031
U 1 1 5A97CE79
P 8100 2750
F 0 "#PWR031" H 8100 2600 50  0001 C CNN
F 1 "+5V0" H 8115 2923 50  0000 C CNN
F 2 "" H 8100 2750 50  0000 C CNN
F 3 "" H 8100 2750 50  0000 C CNN
	1    8100 2750
	1    0    0    -1  
$EndComp
$Comp
L nexperia:BAS70 D10
U 1 1 5A97CE81
P 8350 2950
F 0 "D10" V 8304 3028 50  0000 L CNN
F 1 "BAS70" V 8395 3028 50  0000 L CNN
F 2 "TO_SMD:SOT-23" H 8400 2750 50  0001 C CNN
F 3 "/ptx/archive/library/datasheets/Diodes/Nexperia_BAS70-Series.pdf" H 8350 3050 50  0001 C CNN
F 4 "Nexperia" H 8450 3150 60  0001 C CNN "Manufacturer"
F 5 "BAS70-215, BAS70-235" H 8550 3250 60  0001 C CNN "MPN"
	1    8350 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	8350 2750 8100 2750
Wire Wire Line
	8100 3150 8350 3150
$Comp
L nexperia:BSS138BK Q5
U 1 1 5A97CE8C
P 8100 3450
F 0 "Q5" H 8191 3553 60  0000 L CNN
F 1 "BSS138BK" H 8191 3447 60  0000 L CNN
F 2 "TO_SMD:SOT-23" H 6350 4850 60  0001 C CNN
F 3 "/ptx/archive/library/datasheets/Transistors/Nexperia_BSS138BK_N-MOSFET.pdf" H 6350 4850 60  0001 C CNN
F 4 "BSS138BK" H 6450 4950 60  0001 C CNN "MPN"
F 5 "Nexperia" H 6550 5050 60  0001 C CNN "Manufacturer"
	1    8100 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 3200 8100 3150
$Comp
L powerport:GND #PWR032
U 1 1 5A97CE94
P 8100 3800
F 0 "#PWR032" H 8100 3700 50  0001 C CNN
F 1 "GND" H 8115 3677 50  0000 C CNN
F 2 "" H 8100 3800 50  0000 C CNN
F 3 "" H 8100 3800 50  0000 C CNN
	1    8100 3800
	1    0    0    -1  
$EndComp
$Comp
L Res_1Percent_E24_0603_100mW_-40C-90C:10k_0603_100mW_1%_E24_Chip-Resistor R15
U 1 1 5A97CE9D
P 7650 3600
F 0 "R15" H 7720 3646 50  0000 L CNN
F 1 "10k_0603_100mW_1%_E24_Chip-Resistor" V 7514 3601 50  0001 C CNN
F 2 "Resistors_SMD:R_0603" V 7580 3600 50  0001 C CNN
F 3 "" V 7730 3600 50  0001 C CNN
F 4 "any" V 7830 3700 60  0001 C CNN "MPN"
F 5 "any" V 7930 3800 60  0001 C CNN "Manufacturer"
F 6 "10k" H 7720 3555 50  0000 L CNN "DisplayValue"
	1    7650 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 3600 8100 3750
Wire Wire Line
	7650 3750 8100 3750
Connection ~ 8100 3750
Wire Wire Line
	8100 3750 8100 3800
Wire Wire Line
	7800 3450 7650 3450
Connection ~ 7650 3450
Wire Wire Line
	7650 3450 7500 3450
Text HLabel 7500 3450 0    50   Input ~ 0
SIG5_IN
$Comp
L Hongfa-Relay:HFD2-005-S-D K1
U 1 1 5A9C2A95
P 2550 1300
F 0 "K1" H 2550 1493 50  0000 C CNN
F 1 "HFD2-005-S-D" H 2550 1050 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 2450 1400 50  0001 C CNN
F 3 "" H 2550 1500 50  0001 C CNN
F 4 "HFD2/005-S-D" H 2650 1600 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 2750 1700 60  0001 C CNN "Manufacturer"
	1    2550 1300
	1    0    0    -1  
$EndComp
$Comp
L Hongfa-Relay:HFD2-005-S-D K1
U 2 1 5A9C2B1E
P 4100 1350
F 0 "K1" V 4100 1418 50  0000 L CNN
F 1 "HFD2-005-S-D" H 4100 1100 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 4000 1450 50  0001 C CNN
F 3 "" H 4100 1550 50  0001 C CNN
F 4 "HFD2/005-S-D" H 4200 1650 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 4300 1750 60  0001 C CNN "Manufacturer"
	2    4100 1350
	0    1    1    0   
$EndComp
Connection ~ 4100 1150
Connection ~ 4100 1550
$Comp
L Hongfa-Relay:HFD2-005-S-D K1
U 3 1 5A9C2C72
P 2550 1750
F 0 "K1" H 2550 1943 50  0000 C CNN
F 1 "HFD2-005-S-D" H 2550 1500 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 2450 1850 50  0001 C CNN
F 3 "" H 2550 1950 50  0001 C CNN
F 4 "HFD2/005-S-D" H 2650 2050 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 2750 2150 60  0001 C CNN "Manufacturer"
	3    2550 1750
	1    0    0    -1  
$EndComp
$Comp
L Hongfa-Relay:HFD2-005-S-D K2
U 1 1 5A9C2DD3
P 2550 2950
F 0 "K2" H 2550 3143 50  0000 C CNN
F 1 "HFD2-005-S-D" H 2550 2700 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 2450 3050 50  0001 C CNN
F 3 "" H 2550 3150 50  0001 C CNN
F 4 "HFD2/005-S-D" H 2650 3250 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 2750 3350 60  0001 C CNN "Manufacturer"
	1    2550 2950
	1    0    0    -1  
$EndComp
$Comp
L Hongfa-Relay:HFD2-005-S-D K2
U 2 1 5A9C2E7D
P 4100 3000
F 0 "K2" V 4100 3068 50  0000 L CNN
F 1 "HFD2-005-S-D" H 4100 2750 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 4000 3100 50  0001 C CNN
F 3 "" H 4100 3200 50  0001 C CNN
F 4 "HFD2/005-S-D" H 4200 3300 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 4300 3400 60  0001 C CNN "Manufacturer"
	2    4100 3000
	0    1    1    0   
$EndComp
Connection ~ 4100 2800
Connection ~ 4100 3200
$Comp
L Hongfa-Relay:HFD2-005-S-D K2
U 3 1 5A9C2F51
P 2550 3400
F 0 "K2" H 2550 3593 50  0000 C CNN
F 1 "HFD2-005-S-D" H 2550 3150 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 2450 3500 50  0001 C CNN
F 3 "" H 2550 3600 50  0001 C CNN
F 4 "HFD2/005-S-D" H 2650 3700 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 2750 3800 60  0001 C CNN "Manufacturer"
	3    2550 3400
	1    0    0    -1  
$EndComp
$Comp
L Hongfa-Relay:HFD2-005-S-D K3
U 1 1 5A9C30A5
P 2550 4500
F 0 "K3" H 2550 4693 50  0000 C CNN
F 1 "HFD2-005-S-D" H 2550 4250 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 2450 4600 50  0001 C CNN
F 3 "" H 2550 4700 50  0001 C CNN
F 4 "HFD2/005-S-D" H 2650 4800 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 2750 4900 60  0001 C CNN "Manufacturer"
	1    2550 4500
	1    0    0    -1  
$EndComp
$Comp
L Hongfa-Relay:HFD2-005-S-D K3
U 2 1 5A9C314C
P 4100 4550
F 0 "K3" V 4100 4618 50  0000 L CNN
F 1 "HFD2-005-S-D" H 4100 4300 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 4000 4650 50  0001 C CNN
F 3 "" H 4100 4750 50  0001 C CNN
F 4 "HFD2/005-S-D" H 4200 4850 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 4300 4950 60  0001 C CNN "Manufacturer"
	2    4100 4550
	0    1    1    0   
$EndComp
Connection ~ 4100 4350
Connection ~ 4100 4750
$Comp
L Hongfa-Relay:HFD2-005-S-D K3
U 3 1 5A9C32A5
P 2550 4950
F 0 "K3" H 2550 5143 50  0000 C CNN
F 1 "HFD2-005-S-D" H 2550 4700 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 2450 5050 50  0001 C CNN
F 3 "" H 2550 5150 50  0001 C CNN
F 4 "HFD2/005-S-D" H 2650 5250 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 2750 5350 60  0001 C CNN "Manufacturer"
	3    2550 4950
	1    0    0    -1  
$EndComp
$Comp
L Hongfa-Relay:HFD2-005-S-D K4
U 1 1 5A9C3B86
P 6550 1350
F 0 "K4" H 6550 1543 50  0000 C CNN
F 1 "HFD2-005-S-D" H 6550 1100 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 6450 1450 50  0001 C CNN
F 3 "" H 6550 1550 50  0001 C CNN
F 4 "HFD2/005-S-D" H 6650 1650 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 6750 1750 60  0001 C CNN "Manufacturer"
	1    6550 1350
	1    0    0    -1  
$EndComp
$Comp
L Hongfa-Relay:HFD2-005-S-D K4
U 2 1 5A9C3C46
P 8100 1400
F 0 "K4" V 8100 1468 50  0000 L CNN
F 1 "HFD2-005-S-D" H 8100 1150 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 8000 1500 50  0001 C CNN
F 3 "" H 8100 1600 50  0001 C CNN
F 4 "HFD2/005-S-D" H 8200 1700 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 8300 1800 60  0001 C CNN "Manufacturer"
	2    8100 1400
	0    1    1    0   
$EndComp
Connection ~ 8100 1200
Connection ~ 8100 1600
$Comp
L Hongfa-Relay:HFD2-005-S-D K4
U 3 1 5A9C3D2A
P 6550 1800
F 0 "K4" H 6550 1993 50  0000 C CNN
F 1 "HFD2-005-S-D" H 6550 1550 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 6450 1900 50  0001 C CNN
F 3 "" H 6550 2000 50  0001 C CNN
F 4 "HFD2/005-S-D" H 6650 2100 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 6750 2200 60  0001 C CNN "Manufacturer"
	3    6550 1800
	1    0    0    -1  
$EndComp
$Comp
L Hongfa-Relay:HFD2-005-S-D K5
U 1 1 5A9C3EA7
P 6550 2900
F 0 "K5" H 6550 3093 50  0000 C CNN
F 1 "HFD2-005-S-D" H 6550 2650 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 6450 3000 50  0001 C CNN
F 3 "" H 6550 3100 50  0001 C CNN
F 4 "HFD2/005-S-D" H 6650 3200 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 6750 3300 60  0001 C CNN "Manufacturer"
	1    6550 2900
	1    0    0    -1  
$EndComp
$Comp
L Hongfa-Relay:HFD2-005-S-D K5
U 2 1 5A9C3F70
P 8100 2950
F 0 "K5" V 8100 3018 50  0000 L CNN
F 1 "HFD2-005-S-D" H 8100 2700 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 8000 3050 50  0001 C CNN
F 3 "" H 8100 3150 50  0001 C CNN
F 4 "HFD2/005-S-D" H 8200 3250 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 8300 3350 60  0001 C CNN "Manufacturer"
	2    8100 2950
	0    1    1    0   
$EndComp
Connection ~ 8100 2750
Connection ~ 8100 3150
$Comp
L Hongfa-Relay:HFD2-005-S-D K5
U 3 1 5A9C4062
P 6550 3350
F 0 "K5" H 6550 3543 50  0000 C CNN
F 1 "HFD2-005-S-D" H 6550 3100 50  0001 C CNN
F 2 "Relay_Hongfa-Relay:HFD2_xxx-S-D" H 6450 3450 50  0001 C CNN
F 3 "" H 6550 3550 50  0001 C CNN
F 4 "HFD2/005-S-D" H 6650 3650 60  0001 C CNN "MPN"
F 5 "Hongfa-Relay" H 6750 3750 60  0001 C CNN "Manufacturer"
	3    6550 3350
	1    0    0    -1  
$EndComp
$Comp
L Cap_10Percent_E24_0603_X7R_50V_-40C-125C:100n_0603_X7R_10%_50V_-40C..125C_Chip-Capacitor C13
U 1 1 5A9F455F
P 4850 4950
F 0 "C13" H 4961 4996 50  0000 L CNN
F 1 "100n_0603_X7R_10%_50V_-40C..125C_Chip-Capacitor" H 4875 4850 50  0001 L CNN
F 2 "Capacitors_SMD:C_0603" H 4888 4800 50  0001 C CNN
F 3 "" H 4990 4715 50  0001 C CNN
F 4 "any" H 4975 5150 60  0001 C CNN "MPN"
F 5 "any" H 5075 5250 60  0001 C CNN "Manufacturer"
F 6 "100n" H 4961 4905 50  0000 L CNN "DisplayValue"
	1    4850 4950
	1    0    0    -1  
$EndComp
$Comp
L powerport:+5V0 #PWR037
U 1 1 5A9F4689
P 4850 4800
F 0 "#PWR037" H 4850 4650 50  0001 C CNN
F 1 "+5V0" H 4865 4973 50  0000 C CNN
F 2 "" H 4850 4800 50  0000 C CNN
F 3 "" H 4850 4800 50  0000 C CNN
	1    4850 4800
	1    0    0    -1  
$EndComp
$Comp
L powerport:GND #PWR038
U 1 1 5A9F46D2
P 4850 5100
F 0 "#PWR038" H 4850 5000 50  0001 C CNN
F 1 "GND" H 4865 4977 50  0000 C CNN
F 2 "" H 4850 5100 50  0000 C CNN
F 3 "" H 4850 5100 50  0000 C CNN
	1    4850 5100
	1    0    0    -1  
$EndComp
$Comp
L Cap_10Percent_E24_0603_X7R_50V_-40C-125C:100n_0603_X7R_10%_50V_-40C..125C_Chip-Capacitor C12
U 1 1 5A9F4800
P 4850 3350
F 0 "C12" H 4961 3396 50  0000 L CNN
F 1 "100n_0603_X7R_10%_50V_-40C..125C_Chip-Capacitor" H 4875 3250 50  0001 L CNN
F 2 "Capacitors_SMD:C_0603" H 4888 3200 50  0001 C CNN
F 3 "" H 4990 3115 50  0001 C CNN
F 4 "any" H 4975 3550 60  0001 C CNN "MPN"
F 5 "any" H 5075 3650 60  0001 C CNN "Manufacturer"
F 6 "100n" H 4961 3305 50  0000 L CNN "DisplayValue"
	1    4850 3350
	1    0    0    -1  
$EndComp
$Comp
L powerport:+5V0 #PWR039
U 1 1 5A9F4807
P 4850 3200
F 0 "#PWR039" H 4850 3050 50  0001 C CNN
F 1 "+5V0" H 4865 3373 50  0000 C CNN
F 2 "" H 4850 3200 50  0000 C CNN
F 3 "" H 4850 3200 50  0000 C CNN
	1    4850 3200
	1    0    0    -1  
$EndComp
$Comp
L powerport:GND #PWR040
U 1 1 5A9F480D
P 4850 3500
F 0 "#PWR040" H 4850 3400 50  0001 C CNN
F 1 "GND" H 4865 3377 50  0000 C CNN
F 2 "" H 4850 3500 50  0000 C CNN
F 3 "" H 4850 3500 50  0000 C CNN
	1    4850 3500
	1    0    0    -1  
$EndComp
$Comp
L Cap_10Percent_E24_0603_X7R_50V_-40C-125C:100n_0603_X7R_10%_50V_-40C..125C_Chip-Capacitor C11
U 1 1 5A9F6FCA
P 4850 1750
F 0 "C11" H 4961 1796 50  0000 L CNN
F 1 "100n_0603_X7R_10%_50V_-40C..125C_Chip-Capacitor" H 4875 1650 50  0001 L CNN
F 2 "Capacitors_SMD:C_0603" H 4888 1600 50  0001 C CNN
F 3 "" H 4990 1515 50  0001 C CNN
F 4 "any" H 4975 1950 60  0001 C CNN "MPN"
F 5 "any" H 5075 2050 60  0001 C CNN "Manufacturer"
F 6 "100n" H 4961 1705 50  0000 L CNN "DisplayValue"
	1    4850 1750
	1    0    0    -1  
$EndComp
$Comp
L powerport:+5V0 #PWR041
U 1 1 5A9F6FD1
P 4850 1600
F 0 "#PWR041" H 4850 1450 50  0001 C CNN
F 1 "+5V0" H 4865 1773 50  0000 C CNN
F 2 "" H 4850 1600 50  0000 C CNN
F 3 "" H 4850 1600 50  0000 C CNN
	1    4850 1600
	1    0    0    -1  
$EndComp
$Comp
L powerport:GND #PWR042
U 1 1 5A9F6FD7
P 4850 1900
F 0 "#PWR042" H 4850 1800 50  0001 C CNN
F 1 "GND" H 4865 1777 50  0000 C CNN
F 2 "" H 4850 1900 50  0000 C CNN
F 3 "" H 4850 1900 50  0000 C CNN
	1    4850 1900
	1    0    0    -1  
$EndComp
$Comp
L Cap_10Percent_E24_0603_X7R_50V_-40C-125C:100n_0603_X7R_10%_50V_-40C..125C_Chip-Capacitor C15
U 1 1 5A9F9816
P 8850 1800
F 0 "C15" H 8961 1846 50  0000 L CNN
F 1 "100n_0603_X7R_10%_50V_-40C..125C_Chip-Capacitor" H 8875 1700 50  0001 L CNN
F 2 "Capacitors_SMD:C_0603" H 8888 1650 50  0001 C CNN
F 3 "" H 8990 1565 50  0001 C CNN
F 4 "any" H 8975 2000 60  0001 C CNN "MPN"
F 5 "any" H 9075 2100 60  0001 C CNN "Manufacturer"
F 6 "100n" H 8961 1755 50  0000 L CNN "DisplayValue"
	1    8850 1800
	1    0    0    -1  
$EndComp
$Comp
L powerport:+5V0 #PWR043
U 1 1 5A9F981D
P 8850 1650
F 0 "#PWR043" H 8850 1500 50  0001 C CNN
F 1 "+5V0" H 8865 1823 50  0000 C CNN
F 2 "" H 8850 1650 50  0000 C CNN
F 3 "" H 8850 1650 50  0000 C CNN
	1    8850 1650
	1    0    0    -1  
$EndComp
$Comp
L powerport:GND #PWR044
U 1 1 5A9F9823
P 8850 1950
F 0 "#PWR044" H 8850 1850 50  0001 C CNN
F 1 "GND" H 8865 1827 50  0000 C CNN
F 2 "" H 8850 1950 50  0000 C CNN
F 3 "" H 8850 1950 50  0000 C CNN
	1    8850 1950
	1    0    0    -1  
$EndComp
$Comp
L Cap_10Percent_E24_0603_X7R_50V_-40C-125C:100n_0603_X7R_10%_50V_-40C..125C_Chip-Capacitor C14
U 1 1 5A9FBFBA
P 8800 3350
F 0 "C14" H 8911 3396 50  0000 L CNN
F 1 "100n_0603_X7R_10%_50V_-40C..125C_Chip-Capacitor" H 8825 3250 50  0001 L CNN
F 2 "Capacitors_SMD:C_0603" H 8838 3200 50  0001 C CNN
F 3 "" H 8940 3115 50  0001 C CNN
F 4 "any" H 8925 3550 60  0001 C CNN "MPN"
F 5 "any" H 9025 3650 60  0001 C CNN "Manufacturer"
F 6 "100n" H 8911 3305 50  0000 L CNN "DisplayValue"
	1    8800 3350
	1    0    0    -1  
$EndComp
$Comp
L powerport:+5V0 #PWR045
U 1 1 5A9FBFC1
P 8800 3200
F 0 "#PWR045" H 8800 3050 50  0001 C CNN
F 1 "+5V0" H 8815 3373 50  0000 C CNN
F 2 "" H 8800 3200 50  0000 C CNN
F 3 "" H 8800 3200 50  0000 C CNN
	1    8800 3200
	1    0    0    -1  
$EndComp
$Comp
L powerport:GND #PWR046
U 1 1 5A9FBFC7
P 8800 3500
F 0 "#PWR046" H 8800 3400 50  0001 C CNN
F 1 "GND" H 8815 3377 50  0000 C CNN
F 2 "" H 8800 3500 50  0000 C CNN
F 3 "" H 8800 3500 50  0000 C CNN
	1    8800 3500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
